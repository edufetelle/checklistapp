//
//  CheckListItem.swift
//  Checklists
//
//  Created by Edouard Dufetelle on 3/22/18.
//  Copyright © 2018 Edouard Dufetelle. All rights reserved.
//

import Foundation
import UserNotifications

class ChecklistItem : Equatable, Codable {
    
    static func ==(lhs: ChecklistItem, rhs: ChecklistItem) -> Bool {
        return lhs.text == rhs.text
    }
    
    static func nextChecklistItemID() -> Int {
        let userDefaults = UserDefaults.standard
        let itemID = userDefaults.integer(forKey: "ChecklistItemID")
        userDefaults.set(itemID + 1, forKey: "ChecklistItemID")
        userDefaults.synchronize()
        return itemID
    }
    
    // MARK:- instance variables
    
    var text: String
    
    var isChecked = false {
        didSet {
            scheduleNotification()
        }
    }
    
    var shouldRemind = false {
        didSet {
            scheduleNotification()
        }
    }
    
    var dueDate = Date() {
        didSet {
            scheduleNotification()
        }
    }
    
    var itemID : Int
    
    
    // MARK:- initializers
    
    init() {
        self.text = ""
        self.itemID = ChecklistItem.nextChecklistItemID()
        scheduleNotification()
    }
    
    init(text: String, isChecked: Bool = false, shouldRemind : Bool = false, dueDate : Date = Date()) {
        self.text = text
        self.isChecked = isChecked
        self.itemID = ChecklistItem.nextChecklistItemID()
        self.shouldRemind = shouldRemind
        self.dueDate = dueDate
        scheduleNotification()
    }
    
    deinit {
        removeNotification()
    }
    
    
    // MARK:- methods
    
    func toggleCheckmark() {
        isChecked = !isChecked
    }
    
    func scheduleNotification() {
        removeNotification()
        if dueDate > Date() && shouldRemind && !isChecked {
            let content = UNMutableNotificationContent()
            content.title = "Reminder:"
            content.body = text
            content.sound = UNNotificationSound.default()
            
            let calendar = Calendar(identifier: .gregorian)
            let components = calendar.dateComponents([.year,.month,.day,.hour,.minute], from: dueDate)
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
            let request = UNNotificationRequest(identifier: "\(itemID)", content: content, trigger: trigger)
            let center = UNUserNotificationCenter.current()
            center.add(request)
        }
    }
    
    func removeNotification() {
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: ["\(itemID)"])
    }
    
}
