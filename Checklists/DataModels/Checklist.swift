//
//  Checklist.swift
//  Checklists
//
//  Created by Edouard Dufetelle on 4/7/18.
//  Copyright © 2018 Edouard Dufetelle. All rights reserved.
//

import Foundation

class Checklist : Equatable, Codable {
    
    static func ==(lhs: Checklist, rhs: Checklist) -> Bool {
        return lhs.name == rhs.name
    }
    
    var name : String
    var items = [ChecklistItem]()
    var iconName : String
    
    init() {
        self.name = ""
        self.iconName = "No Icon"
    }
    
    init(name : String, iconName : String = "Folder") {
        self.name = name
        self.iconName = iconName
    }
    
    func countRemainingItems() -> Int {
        var count = 0
        for item in items where !item.isChecked {
            count += 1
        }
        return count
    }
    
}
