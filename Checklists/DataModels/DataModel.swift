//
//  DataModel.swift
//  Checklists
//
//  Created by Edouard Dufetelle on 4/15/18.
//  Copyright © 2018 Edouard Dufetelle. All rights reserved.
//

import Foundation

class DataModel {
    
    var lists = [Checklist]()
    
    var indexOfLastOpenedList : Int {
        get {
            return UserDefaults.standard.integer(forKey: "CheckList")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "CheckList")
            UserDefaults.standard.synchronize()
        }
    }
    
    init() {
        loadChecklists()
        registerDefaults()
        handleFirstTime()
    }
    
    func sortChecklists() {
        lists.sort(by: { checklist1, checklist2 in return checklist1.name.localizedCompare(checklist2.name) == .orderedAscending })
    }
    
    
    //MARK:- UserDefaults
    
    private func registerDefaults() {
        UserDefaults.standard.register(defaults: ["CheckList" : -1, "FirstTime" : true])
    }
    
    private func handleFirstTime() {
        if UserDefaults.standard.bool(forKey: "FirstTime") {
            let checklist = Checklist(name: "List")
            lists.append(checklist)
            indexOfLastOpenedList = 0
            UserDefaults.standard.set(false, forKey: "FirstTime")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    //MARK: - saving and loading
    
    private func documentDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    private func dataFilePath() -> URL {
        return documentDirectory().appendingPathComponent("Checklists.plist")
    }
    
    func saveChecklists() {
        let encoder = PropertyListEncoder()
        do {
            let data = try encoder.encode(lists)
            try data.write(to: dataFilePath(), options: Data.WritingOptions.atomic)
        } catch {
            print("Error encoding checklists array!")
        }
    }
    
    func loadChecklists() {
        let path = dataFilePath()
        if let data = try? Data(contentsOf: path) {
            let decoder = PropertyListDecoder()
            do {
                lists = try decoder.decode([Checklist].self, from: data)
                sortChecklists()
            } catch {
                print("Error decoding checklists array!")
            }
        }
    }
    
}
