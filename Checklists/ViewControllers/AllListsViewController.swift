//
//  AllListsViewController.swift
//  Checklists
//
//  Created by Edouard Dufetelle on 4/7/18.
//  Copyright © 2018 Edouard Dufetelle. All rights reserved.
//

import UIKit

class AllListsViewController: UITableViewController, ListDetailViewControllerDelegate, UINavigationControllerDelegate, ChecklistViewControllerDelegate {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.delegate = self
        let index = dataModel.indexOfLastOpenedList
        if index >= 0 && index < dataModel.lists.count {
            let checkList = dataModel.lists[index]
            performSegue(withIdentifier: "ShowChecklist", sender: checkList)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        for i in 0..<dataModel.lists.count {
//            if let cell = tableView.cellForRow(at: IndexPath(row: i, section: 0)) {
//                let checklist = dataModel.lists[i]
//                configureDetailTextLabel(for: cell, with: checklist)
//            }
//        }
    }
    
    
    // MARK:- ListDetailViewControllerDelegate Protocol implementation
    
    func listDetailViewControllerDidCancel(_ controller: ListDetailViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func listDetailViewController(_ controller: ListDetailViewController, didFinishAdddingItem item: Checklist) {
        dataModel.lists.append(item)
        dataModel.sortChecklists()
        let indexPath = IndexPath(row: dataModel.lists.index(of: item)!, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
        navigationController?.popViewController(animated: true)
    }
    
    func listDetailViewController(_ controller: ListDetailViewController, didFinishEditing item: Checklist) {
        let initialIndex = dataModel.lists.index(of: item)!
        dataModel.sortChecklists()
        let newIndex = dataModel.lists.index(of: item)!
        for i in min(initialIndex, newIndex)...max(initialIndex, newIndex) {
            let cell = tableView.cellForRow(at: IndexPath(row: i, section: 0))!
            let checklist = dataModel.lists[i]
            updateCellContent(for: cell, with: checklist)
        }
//        let cell = tableView.cellForRow(at: IndexPath(row: initialIndex, section: 0))
//        cell?.textLabel?.text = item.name
        navigationController?.popViewController(animated: true)
    }
    

    // MARK:- UINavigationControllerDelegate Protocol implementation
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController === self {
            dataModel.indexOfLastOpenedList = -1
        }
    }
    
    
    // MARK:- ChecklistViewControllerDelegate Protocol implementation
    
    func checklistViewControllerDidChangeNumberOfCheckmarks(_ controller: ChecklistViewController, for checklist : Checklist) {
        configureDetailTextLabel(for: tableView.cellForRow(at: IndexPath(row: dataModel.lists.index(of: checklist)!, section: 0))!, with: checklist)
    }
    
    
    // MARK:- instance variables
    
    var dataModel: DataModel!
    
    
    // MARK: - Table view methods implementation

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.lists.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = makeCell(for: tableView)
        let checklist = dataModel.lists[indexPath.row]
        cell.accessoryType = .detailDisclosureButton
        updateCellContent(for: cell, with: checklist)
        return cell
    }
    
    func makeCell(for tableView: UITableView) -> UITableViewCell {
        let cellIdentifier = "Cell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) {
            return cell
        } else {
            return UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
    }
    
    func configureDetailTextLabel(for cell: UITableViewCell, with checklist: Checklist) {
        if checklist.items.isEmpty {
            cell.detailTextLabel?.text = "(No item)"
        } else if checklist.countRemainingItems() == 0 {
            cell.detailTextLabel?.text = "All done!"
        } else {
            cell.detailTextLabel?.text = "\(checklist.countRemainingItems()) remaining"
        }
    }
    
    func updateCellContent(for cell: UITableViewCell, with checklist: Checklist) {
        cell.textLabel?.text = checklist.name
        configureDetailTextLabel(for: cell, with: checklist)
        cell.imageView?.image = UIImage(named: checklist.iconName)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        //Delete the row from the data source
        dataModel.lists.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        dataModel.indexOfLastOpenedList = indexPath.row
        performSegue(withIdentifier: "ShowChecklist", sender: dataModel.lists[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        performSegue(withIdentifier: "EditChecklist", sender: dataModel.lists[indexPath.row])
    }
    
    
    // MARK:- Segue preparation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowChecklist" {
            let controller = segue.destination as! ChecklistViewController
            controller.delegate = self
            controller.checklist = sender as! Checklist
        } else if segue.identifier == "EditChecklist" {
            let controller = segue.destination as! ListDetailViewController
            let checklist = sender as! Checklist
            controller.delegate = self
            controller.listToEdit = checklist
        } else if segue.identifier == "AddChecklist" {
            let controller = segue.destination as! ListDetailViewController
            controller.delegate = self
        }
    }
    
    
    
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    

 

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
