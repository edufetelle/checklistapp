//
//  ViewController.swift
//  Checklists
//
//  Created by Edouard Dufetelle on 3/20/18.
//  Copyright © 2018 Edouard Dufetelle. All rights reserved.
//

import UIKit

protocol ChecklistViewControllerDelegate: class {
    func checklistViewControllerDidChangeNumberOfCheckmarks(_ controller: ChecklistViewController, for checklist : Checklist)
}

class ChecklistViewController: UITableViewController, ItemDetailViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = checklist.name
        navigationItem.largeTitleDisplayMode = .never
//        loadChecklistItems()
//        print("Document Folder is \(documentDirectory())")
//        print("Data file path is \(dataFilePath())")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - instance variables
    
    var checklist : Checklist!
    weak var delegate : ChecklistViewControllerDelegate?
    
    
    // MARK: - tableView functions implementation
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checklist.items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChecklistItem", for: indexPath)
        let item = checklist.items[indexPath.row]
        configureText(for: cell, with: item)
        configureCheckmark(for: cell, with: item)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            let item = checklist.items[indexPath.row]
            item.toggleCheckmark()
            configureCheckmark(for: cell, with: item)
            delegate?.checklistViewControllerDidChangeNumberOfCheckmarks(self, for: checklist)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        checklist.items.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        delegate?.checklistViewControllerDidChangeNumberOfCheckmarks(self, for: checklist)
    }
    
    
    // MARK: - Segue preparation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddItem" {
            let controller = segue.destination as! ItemDetailViewController
            controller.delegate = self
        } else if segue.identifier == "EditItem" {
            let controller = segue.destination as! ItemDetailViewController
            controller.delegate = self
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell) {
                controller.itemToEdit = checklist.items[indexPath.row]
            }
        }
    }
    
    
    // MARK: - display updating functions
    
    func configureCheckmark(for cell: UITableViewCell, with item: ChecklistItem) {
        let checkMark = cell.viewWithTag(1001) as! UILabel
        checkMark.text = item.isChecked ? "✓" : ""
    }
    
    func configureText(for cell: UITableViewCell, with item: ChecklistItem) {
        let label = cell.viewWithTag(1000) as! UILabel
        label.text = item.text
    }
    
    
    //MARK: - protocol implementation
    
    func itemDetailViewControllerDidCancel(_ controller: ItemDetailViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishAdddingItem item: ChecklistItem) {
        checklist.items.append(item)
        let indexPath = IndexPath(row: tableView.numberOfRows(inSection: 0), section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
        delegate?.checklistViewControllerDidChangeNumberOfCheckmarks(self, for: checklist)
        navigationController?.popViewController(animated: true)
    }
    
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishEditing item: ChecklistItem) {
        if let index = checklist.items.index(of: item) {
            if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) {
                configureText(for: cell, with: item)
            }
        }
        navigationController?.popViewController(animated: true)
    }
    
/*
    //MARK: - saving and loading
    
    func documentDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func dataFilePath() -> URL {
        return documentDirectory().appendingPathComponent("Checklists.plist")
    }
    
    func saveChecklistItems() {
        let encoder = PropertyListEncoder()
        do {
            let data = try encoder.encode(checklistItems)
            try data.write(to: dataFilePath(), options: Data.WritingOptions.atomic)
        } catch {
            print("Error encoding checklist item array!")
        }
    }
    
    func loadChecklistItems() {
        let path = dataFilePath()
        
        if let data = try? Data(contentsOf: path) {
            let decoder = PropertyListDecoder()
            do {
                checklistItems = try decoder.decode([ChecklistItem].self, from: data)
            } catch {
                print("Error decoding item array!")
            }
        }
    }
 
 */
    
}

