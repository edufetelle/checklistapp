//
//  ListDetailViewController.swift
//  Checklists
//
//  Created by Edouard Dufetelle on 4/10/18.
//  Copyright © 2018 Edouard Dufetelle. All rights reserved.
//

import UIKit

protocol ListDetailViewControllerDelegate: class {
    func listDetailViewControllerDidCancel(_ controller: ListDetailViewController)
    func listDetailViewController(_ controller: ListDetailViewController, didFinishAdddingItem item: Checklist)
    func listDetailViewController(_ controller: ListDetailViewController, didFinishEditing item: Checklist)
}

class ListDetailViewController: UITableViewController, UITextFieldDelegate, IconPickerViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        if let list = listToEdit {
            navigationItem.title = "Edit List"
            textField.text = list.name
            doneBarButton.isEnabled = true
            iconName = listToEdit!.iconName
        }
        iconImageView.image = UIImage(named: iconName)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textField.becomeFirstResponder()
    }
    
    // MARK: - textField delegate method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let oldText = textField.text!
        let stringRange = Range(range, in:oldText)!
        let newText = oldText.replacingCharacters(in: stringRange, with: string)
        doneBarButton.isEnabled = !newText.isEmpty
        return true
    }
    
    
    // MARK: - instance variables
    
    weak var delegate: ListDetailViewControllerDelegate?
    var listToEdit : Checklist?
    var iconName = "Folder"
    
    
    // MARK: - outlets and actions
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBAction func cancel() {
        delegate?.listDetailViewControllerDidCancel(self)
    }
    
    @IBAction func done() {
        if listToEdit != nil {
            listToEdit!.name = textField.text!
            listToEdit!.iconName = iconName
            delegate?.listDetailViewController(self, didFinishEditing: listToEdit!)
        } else {
            let list = Checklist(name: textField.text!, iconName: iconName)
            delegate?.listDetailViewController(self, didFinishAdddingItem: list)
        }
    }
    
    
    // MARK: - tableView method implementation
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath.section == 1 ? indexPath : nil
    }
    
    
    // MARK:- iconPicker protocol implementation
    func iconPicker(_ picker: IconPickerViewController, didPick iconName: String) {
        self.iconName = iconName
        iconImageView.image = UIImage(named: iconName)
        navigationController?.popViewController(animated: true)
    }
    
    // Segue preparation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Pick Icon" {
            let controller = segue.destination as! IconPickerViewController
            controller.delegate = self
        }
    }
    
}
